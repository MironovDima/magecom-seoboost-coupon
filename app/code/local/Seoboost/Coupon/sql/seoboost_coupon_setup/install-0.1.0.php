<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('salesrule'),
    'date_end_coupon',
    [
        'type' => Varien_Db_Ddl_Table::TYPE_BIGINT,
        'nullable' => true,
        'length' => 50,
        'comment' => 'date_end_coupon  in UNIX'
    ]
);

$installer->getConnection()->changeColumn(
    $installer->getTable('salesrule'),
    'to_date',
    'to_date',
    [
        'type' => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'nullable' => true,
        'length' => 6,
        'comment' => 'datetime'
    ]
);

$installer->endSetup();
