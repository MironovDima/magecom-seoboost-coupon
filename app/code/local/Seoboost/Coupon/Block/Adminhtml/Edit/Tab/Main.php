<?php

/**
 * @author Mironov Dmitrii <afer-18@list.ru>
 * @copyright Copyright (c) 2017, Mironov Dmitrii
 */
class Seoboost_Coupon_Block_Adminhtml_Edit_Tab_Main
    extends Mage_Adminhtml_Block_Promo_Quote_Edit_Tab_Main
{
    /**
     * {@inheritdoc}
     */
    protected function _prepareForm()
    {
        $model = Mage::registry('current_promo_quote_rule');

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('rule_');

        $fieldset = $form->addFieldset('base_fieldset',
            array('legend' => Mage::helper('salesrule')->__('General Information'))
        );

        if ($model->getId()) {
            $fieldset->addField(
                'rule_id',
                'hidden', [
                    'name' => 'rule_id',
                ]
            );
        }

        $fieldset->addField(
            'product_ids',
            'hidden',
            [
                'name' => 'product_ids',
            ]
        );

        $fieldset->addField(
            'name',
            'text',
            [
                'name' => 'name',
                'label' => Mage::helper('salesrule')->__('Rule Name'),
                'title' => Mage::helper('salesrule')->__('Rule Name'),
                'required' => true,
            ]
        );

        $fieldset->addField(
            'description',
            'textarea',
            [
                'name' => 'description',
                'label' => Mage::helper('salesrule')->__('Description'),
                'title' => Mage::helper('salesrule')->__('Description'),
                'style' => 'height: 100px;',
            ]);

        $field = $fieldset->addField(
            'is_active',
            'select',
            [
                'label' => Mage::helper('salesrule')->__('Status'),
                'title' => Mage::helper('salesrule')->__('Status'),
                'name' => 'is_active',
                'required' => true,
                'options' => [
                    '1' => Mage::helper('salesrule')->__('Active'),
                    '0' => Mage::helper('salesrule')->__('Inactive'),
                ],
            ]
        );

        // script
        $field->setAfterElementHtml("<script type='text/javascript'>
window.onload =function() {
  document.getElementById('rule_date_end_coupon').parentNode.parentNode.style.display='none';

}

function changeDateType() {

  if(document.querySelectorAll('#rule_typeDate option')[document.getElementById('rule_typeDate').selectedIndex].value ==1){
document.getElementById('rule_date_end_coupon').value='';
document.getElementById('rule_date_end_coupon').parentNode.parentNode.style.display ='none';
document.getElementById('rule_to_date').parentNode.parentNode.style.display ='';
} else {
document.getElementById('rule_to_date').value='';
document.getElementById('rule_to_date').parentNode.parentNode.style.display ='none';
document.getElementById('rule_date_end_coupon').parentNode.parentNode.style.display ='';
}
}


</script>");


        if (!$model->getId()) {
            $model->setData('is_active', '1');
        }

        if (Mage::app()->isSingleStoreMode()) {
            $websiteId = Mage::app()->getStore(true)->getWebsiteId();

            $fieldset->addField(
                'website_ids',
                'hidden',
                [
                    'name' => 'website_ids[]',
                    'value' => $websiteId
                ]
            );

            $model->setWebsiteIds($websiteId);
        } else {
            $fieldset->addField(
                'website_ids',
                'multiselect',
                [
                    'name' => 'website_ids[]',
                    'label' => Mage::helper('salesrule')->__('Websites'),
                    'title' => Mage::helper('salesrule')->__('Websites'),
                    'required' => true,
                    'values' => Mage::getSingleton('adminhtml/system_store')->getWebsiteValuesForForm(),
                    'renderer' => 'adminhtml/store_switcher_form_renderer_fieldset_element'
                ]
            );

        }


        $customerGroups = Mage::getResourceModel('customer/group_collection')->load()->toOptionArray();
        $found = false;

        foreach ($customerGroups as $group) {
            if ($group['value'] == 0) {
                $found = true;
            }
        }
        if (!$found) {
            array_unshift(
                $customerGroups,
                [
                    'value' => 0,
                    'label' => Mage::helper('salesrule')->__('NOT LOGGED IN')
                ]
            );
        }

        $fieldset->addField(
            'customer_group_ids',
            'multiselect',
            [
                'name' => 'customer_group_ids[]',
                'label' => Mage::helper('salesrule')->__('Customer Groups'),
                'title' => Mage::helper('salesrule')->__('Customer Groups'),
                'required' => true,
                'values' => Mage::getResourceModel('customer/group_collection')->toOptionArray(),
            ]
        );


        $couponCodeFiled = $fieldset->addField('coupon_code', 'text', [
            'name' => 'coupon_code',
            'label' => Mage::helper('salesrule')->__('Coupon Code'),
            'required' => true,
        ]);

        $couponTypeFiled = $fieldset->addField('coupon_type', 'select', [
            'name' => 'coupon_type',
            'label' => Mage::helper('salesrule')->__('Coupon'),
            'required' => true,
            'options' => Mage::getModel('salesrule/rule')->getCouponTypes(),
        ]);


        $autoGenerationCheckbox = $fieldset->addField('use_auto_generation', 'checkbox', [
            'name' => 'use_auto_generation',
            'label' => Mage::helper('salesrule')->__('Use Auto Generation'),
            'note' => Mage::helper('salesrule')->__('If you select and save the rule you will be able to generate multiple coupon codes.'),
            'onclick' => 'handleCouponsTabContentActivity()',
            'checked' => (int)$model->getUseAutoGeneration() > 0 ? 'checked' : '',
            'renderer' => 'adminhtml/promo_quote_edit_tab_main_renderer_checkbox'
        ]);

        $usesPerCouponFiled = $fieldset->addField('uses_per_coupon', 'text', [
            'name' => 'uses_per_coupon',
            'label' => Mage::helper('salesrule')->__('Uses per Coupon'),
        ]);

        $fieldset->addField(
            'uses_per_customer',
            'text',
            [
                'name' => 'uses_per_customer',
                'label' => Mage::helper('salesrule')->__('Uses per Customer'),
                'note' => Mage::helper('salesrule')->__('Usage limit enforced for logged in customers only'),
            ]
        );


        $fieldset->addField(
            'typeDate',
            'select',
            [
                'name' => 'typeDate',
                'label' => Mage::helper('salesrule')->__('Дата истечения купона'),
                'title' => Mage::helper('salesrule')->__('Дата истечения купона'),
                'required' => false,
                'onchange' => 'changeDateType()',
                'values' => [
                    '1' => Mage::helper('salesrule')->__('Конечной датой'),
                    '2' => Mage::helper('salesrule')->__('Указать в часах')
                ],
            ]
        );


        $fieldset->addField(
            'date_end_coupon',
            'text',
            [
                'name' => 'date_end_coupon',
                'label' => Mage::helper('salesrule')->__('Часов до окончания купона'),
                'title' => Mage::helper('salesrule')->__('time in hours'),
                'required' => false
            ]
        );


        if (intval(strtotime($model->getToDate())) > time()) {
            $dateEnd = strtotime($model->getToDate()) - time();
            $dateEndHour = intval($dateEnd / 60 / 60);
        }

        $model->setData('date_end_coupon', $dateEndHour);


        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);

        $fieldset->addField(
            'from_date',
            'date',
            [
                'name' => 'from_date',
                'label' => Mage::helper('salesrule')->__('From Date'),
                'title' => Mage::helper('salesrule')->__('From Date'),
                'image' => $this->getSkinUrl('images/grid-cal.gif'),
                'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
                'format' => $dateFormatIso
            ]
        );

        $fieldset->addField(
            'to_date',
            'date',
            [
                'name' => 'to_date',
                'label' => Mage::helper('salesrule')->__('To Date'),
                'title' => Mage::helper('salesrule')->__('To Date'),
                'image' => $this->getSkinUrl('images/grid-cal.gif'),
                'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
                'format' => $dateFormatIso
            ]
        );

        $fieldset->addField(
            'sort_order',
            'text',
            [
                'name' => 'sort_order',
                'label' => Mage::helper('salesrule')->__('Priority'),
            ]
        );

        $fieldset->addField(
            'is_rss',
            'select',
            [
                'label' => Mage::helper('salesrule')->__('Public In RSS Feed'),
                'title' => Mage::helper('salesrule')->__('Public In RSS Feed'),
                'name' => 'is_rss',
                'options' => [
                    '1' => Mage::helper('salesrule')->__('Yes'),
                    '0' => Mage::helper('salesrule')->__('No'),
                ],
            ]
        );

        if (!$model->getId()) {
            $model->setIsRss(1);
        }

        $form->setValues($model->getData());

        $autoGenerationCheckbox->setValue(1);

        if ($model->isReadonly()) {
            foreach ($fieldset->getElements() as $element) {
                $element->setReadonly(true, true);
            }
        }

        $this->setForm($form);

        $this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
            ->addFieldMap($couponTypeFiled->getHtmlId(), $couponTypeFiled->getName())
            ->addFieldMap($couponCodeFiled->getHtmlId(), $couponCodeFiled->getName())
            ->addFieldMap($autoGenerationCheckbox->getHtmlId(), $autoGenerationCheckbox->getName())
            ->addFieldMap($usesPerCouponFiled->getHtmlId(), $usesPerCouponFiled->getName())
            ->addFieldDependence(
                $couponCodeFiled->getName(),
                $couponTypeFiled->getName(),
                Mage_SalesRule_Model_Rule::COUPON_TYPE_SPECIFIC)
            ->addFieldDependence(
                $autoGenerationCheckbox->getName(),
                $couponTypeFiled->getName(),
                Mage_SalesRule_Model_Rule::COUPON_TYPE_SPECIFIC)
            ->addFieldDependence(
                $usesPerCouponFiled->getName(),
                $couponTypeFiled->getName(),
                Mage_SalesRule_Model_Rule::COUPON_TYPE_SPECIFIC)
        );
    }
}
