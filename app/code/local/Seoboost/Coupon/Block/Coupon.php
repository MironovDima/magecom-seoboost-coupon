<?php

/**
 * @author Mironov Dmitrii <afer-18@list.ru>
 * @copyright Copyright (c) 2017, Mironov Dmitrii
 */
class Seoboost_Coupon_Block_Coupon extends Mage_Core_Block_Template
{
    /**
     *@return array Coupon data from cookie or generate new coupon
     */
    public function getCouponData()
    {
         return !empty(Mage::helper('seoboost_coupon')->getCustomerCouponSession())
                ? Mage::helper('seoboost_coupon')->getCustomerCouponSession()
                : Mage::helper('seoboost_coupon')->generateCoupon();
    }

    /**
     * @return bool
     */
    public function isEnableCoupon()
    {
        return Mage::getStoreConfig('seoboost_coupon/general/enabled')
                && Mage::helper('seoboost_coupon')->isAutorize()
                && !Mage::helper('seoboost_coupon')->checkProductInStock() ? true : false ;
    }
}
