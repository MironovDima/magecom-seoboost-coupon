<?php

/**
 * @author Mironov Dmitrii <afer-18@list.ru>
 * @copyright Copyright (c) 2017, Mironov Dmitrii
 */
class Seoboost_Coupon_Helper_Data extends Mage_Core_Helper_Data
{
    const GENERATE_LENGTH_COUPON = 12;
    const DEFAULT_PERSENT = 4;
    const UNIX_TWO_DAYS = 172800;
    const PREFIX_COUPON = 'coupon';
    const TWO_DAYS_HOUR = 48;


    /**
     * @var
     */
    public $sessionData;

    /**
     * @return string
     */
    public function getSessionData()
    {
        return Mage::getSingleton('core/session')->getData(self::PREFIX_COUPON);
    }

    function __construct()
    {
        $sessionData = Mage::getSingleton('core/session')->getData(self::PREFIX_COUPON);
        if (empty($sessionData)) {
            Mage::getSingleton('core/session')->setData(self::PREFIX_COUPON, []);
        }
    }

    function __destruct()
    {
        Mage::getSingleton('core/session')->setData(self::PREFIX_COUPON, []);
    }


    /**
     * @param null $length
     * @return string
     */
    public function generateUniqueId($length = null)
    {
        $rndId = crypt(uniqid(rand(), 1));
        $rndId = strip_tags(stripslashes($rndId));
        $rndId = str_replace(array(".", "$"), "", $rndId);
        $rndId = strrev(str_replace("/", "", $rndId));

        if (!is_null($rndId)) {

            return strtoupper(substr($rndId, 0, $length));
        }

        return strtoupper($rndId);
    }

    /**
     * @return int
     */
    private function getCouponPersent()
    {
        return (Mage::getStoreConfig('seoboost_coupon/general/coupon_persent'))
            ? Mage::getStoreConfig('seoboost_coupon/general/coupon_persent')
            : self::DEFAULT_PERSENT;

    }

    /**
     * @return int
     */
    public function getTimeHourCoupon()
    {
        return Mage::getStoreConfig('seoboost_coupon/general/coupon_live');
    }

    /**
     * @param string $hours
     */
    private function setTimeHourCoupon($hours)
    {
        Mage::getConfig()->saveConfig("seoboost_coupon/general/coupon_live", $hours);
    }

    /**
     * @return mixed
     */
    public function checkProductInStock()
    {
        $productId = Mage::app()->getRequest()->getParam('id');
        $stockItem = Mage::getModel('cataloginventory/stock_item')
            ->loadByProduct($productId)->getIsInStock();

        return $stockItem;
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return Mage::getSingleton('customer/session')->getId();
    }

    /**
     * @return bool
     */
    public function isAutorize()
    {
        return Mage::getSingleton('customer/session')->isLoggedIn();
    }

    /**
     *
     * @param $relatedProductsIds
     * @return array
     */
    public function getRelatedProductName($relatedProductsIds)
    {
        $relatedProducts = [];
        if ($relatedProductsIds) {

            foreach ($relatedProductsIds as $relatedProductId) {
                $product = Mage::getModel('catalog/product')->load($relatedProductId);
                $relatedProducts[$product->getProductUrl()] = $product->getName();

            }
            return $relatedProducts;
        }

        return false;
    }

    /**
     *
     * @param $upSellProductsIds
     * @return array
     */
    public function getUpSellProductName($upSellProductsIds)
    {
        $upSellProducts = [];
        if ($upSellProductsIds) {
            foreach ($upSellProductsIds as $upSellProductId) {
                $product = Mage::getModel('catalog/product')->load($upSellProductId);
                $upSellProducts[$product->getProductUrl()] = $product->getName();
            }

            return $upSellProducts;
        }

        return $upSellProducts;
    }

    /**
     * @param array $dataCoupon
     */
    private function setCustomerCouponSession($dataCoupon)
    {
        Mage::getSingleton('core/session')->setData(Mage::app()->getRequest()->getParam('id'), serialize($dataCoupon));
    }

    /**
     * @return mixed
     */
    public function getCustomerCouponSession()
    {
        return unserialize(Mage::getSingleton('core/session')->getData(Mage::app()->getRequest()->getParam('id')));
    }

    /**
     * @return array
     */
    public function generateCoupon()
    {
        $storeId = Mage::app()->getStore()->getId();

        $customer = Mage::getSingleton('customer/session')->getCustomer();

        $groupId = Mage::getSingleton('customer/session')->getCustomerGroupId();


        $productId = Mage::app()->getRequest()->getParam('id');
        $product = Mage::getModel('catalog/product')
            ->setStoreId($storeId)
            ->load($productId);

        $product->getRelatedProductsSku();

        $relatedProductsId = $product->getRelatedProductIds();
        $upSellProductsId = $product->getUpSellProductIds();

        $upSellProductsName = $this->getUpSellProductName($upSellProductsId);
        $relatedProductsName = $this->getRelatedProductName($relatedProductsId);

        $productsIds = array_unique(array_merge($relatedProductsId, $upSellProductsId));

        $productsSku = [];
        $data = [];

        if (!empty($productsIds)) {
            foreach ($productsIds as $productId) {
                $productsSku[] = Mage::getModel('catalog/product')->load($productId)->getSku();
            }
            $productsSku = implode(',', $productsSku);

            /** @var Mage_SalesRule_Model_Coupon_Codegenerator $generator */
            $generator = Mage::getModel('salesrule/coupon_codegenerator')
                ->setLength(self::GENERATE_LENGTH_COUPON);

            /** @var Mage_SalesRule_Model_Rule_Condition_Product $conditionProduct */
            $conditionProduct = Mage::getModel('salesrule/rule_condition_product')
                ->setType('salesrule/rule_condition_product')
                ->setAttribute('sku')
                ->setOperator('()')
                ->setValue($productsSku);

            $conditionProductFound = Mage::getModel('salesrule/rule_condition_product_found')
                ->setConditions(array($conditionProduct));

            /** @var Mage_SalesRule_Model_Rule_Condition_Combine $condition */
            $condition = Mage::getModel('salesrule/rule_condition_combine')
                ->setConditions(array($conditionProductFound));

            /** @var Mage_SalesRule_Model_Coupon $coupon */
            $coupon = Mage::getModel('salesrule/coupon');

            $attempts = 0;
            do {
                if ($attempts++ >= self::GENERATE_LENGTH_COUPON) {
                    Mage::throwException(
                        Mage::helper('seoboost_coupon')->__('Unable to create requested Coupons. Please try again.')
                    );
                }

                $code = $generator->generateCode();
            } while ($coupon->getResource()->exists($code));
            $rule = Mage::getModel('salesrule/rule');

            $rule->setName(Mage::helper('seoboost_coupon')->__(mt_rand(27, 99999) . 'Coupon for-' . mt_rand(15, 999)))
                ->setDescription($rule->getName())
                ->setFromDate(date('Y-m-d'))
                ->setToDate(date("Y-m-d H:i:s", strtotime("+" . $this->getTimeHourCoupon() . " hours")))
                ->setCustomerGroupIds($groupId)
                ->setIsActive(1)
                ->setDateEndCoupon(strtotime("+" . $this->getTimeHourCoupon() . " hours"))
                ->setConditionsSerialized(serialize($condition->asArray()))
                ->setSimpleAction(Mage_SalesRule_Model_Rule::BY_PERCENT_ACTION)
                ->setDiscountAmount($this->getCouponPersent())
                ->setDiscountQty(1)
                ->setStopRulesProcessing(0)
                ->setIsRss(0)
                ->setWebsiteIds(array($storeId))
                ->setCouponType(Mage_SalesRule_Model_Rule::COUPON_TYPE_SPECIFIC)
                ->setConditions($condition)
                ->save();

            $coupon->setId(null)
                ->setRuleId($rule->getRuleId())
                ->setCode($code)
                ->setUsageLimit(1)
                ->setUsagePerCustomer(1)
                ->setIsPrimary(1)
                ->setCreatedAt(time())
                ->setType(Mage_SalesRule_Helper_Coupon::COUPON_FORMAT_ALPHANUMERIC)
                ->save();

            $data = [
                'code' => $code,
                'related' => $relatedProductsName,
                'upsell' => $upSellProductsName,
                'discount' => $this->getCouponPersent()
            ];
        }

        $this->setCustomerCouponSession($data);

        return $data;
    }
}
