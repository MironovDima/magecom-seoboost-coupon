<?php
require_once "Mage/Adminhtml/controllers/Promo/QuoteController.php";

class Seoboost_Coupon_Adminhtml_Promo_QuoteController extends Mage_Adminhtml_Promo_QuoteController
{

    public function postDispatch()
    {
        parent::postDispatch();
        Mage::dispatchEvent('controller_action_postdispatch_adminhtml', array('controller_action' => $this));
    }

    public function validateDataNew(Varien_Object $object)
    {
        $result = array();
        $fromDate = $toDate = null;

        if ($object->hasFromDate() && $object->hasToDate()) {
            $fromDate = $object->getFromDate();
            $toDate = $object->getToDate();
        }

        if ($fromDate && $toDate) {
            $fromDate = new Zend_Date($fromDate, Varien_Date::DATE_INTERNAL_FORMAT);
            $toDate = new Zend_Date($toDate, Varien_Date::DATETIME_PHP_FORMAT);

            if (strtotime($fromDate) > strtotime($toDate)) {
                $result[] = Mage::helper('rule')->__('End Date must be greater than Start Date.');
            }
        }

        if ($object->hasWebsiteIds()) {
            $websiteIds = $object->getWebsiteIds();
            if (empty($websiteIds)) {
                $result[] = Mage::helper('rule')->__('Websites must be specified.');
            }
        }
        if ($object->hasCustomerGroupIds()) {
            $customerGroupIds = $object->getCustomerGroupIds();
            if (empty($customerGroupIds)) {
                $result[] = Mage::helper('rule')->__('Customer Groups must be specified.');
            }
        }

        return !empty($result) ? $result : true;
    }


    public function saveAction()
    {
        if ($this->getRequest()->getPost()) {
            try {
                /** @var $model Mage_SalesRule_Model_Rule */
                $model = Mage::getModel('salesrule/rule');
                Mage::dispatchEvent(
                    'adminhtml_controller_salesrule_prepare_save',
                    ['request' => $this->getRequest()]
                );
                $data = $this->getRequest()->getPost();
                $data = $this->_filterDates($data, array('from_date'));
                $id = $this->getRequest()->getParam('rule_id');
                if ($id) {
                    $model->load($id);
                    if ($id !== $model->getId()) {
                        Mage::throwException(Mage::helper('salesrule')->__('Wrong rule specified.'));
                    }
                }

                $isHourDate = false;

                if (!empty(abs($data['date_end_coupon'])) && abs($data['date_end_coupon']) > 0 && intval($data['typeDate']) == 2) {
                    $isHourDate = true;

                    $data['date_end_coupon'] = strtotime("+" . $data['date_end_coupon'] . " hours");
                    $data['to_date'] = date("Y-m-d H:i:s", $data['date_end_coupon']);
                }

                $session = Mage::getSingleton('adminhtml/session');

                $validateResult = $isHourDate
                    ? $this->validateDataNew(new Varien_Object($data))
                    : $model->validateData(new Varien_Object($data));

                if (!$validateResult) {

                    foreach ($validateResult as $errorMessage) {
                        $session->addError($errorMessage);
                    }

                    $session->setPageData($data);
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));

                    return;
                }

                if (isset($data['simple_action']) && $data['simple_action'] == 'by_percent'
                    && isset($data['discount_amount'])
                ) {
                    $data['discount_amount'] = min(100, $data['discount_amount']);
                }

                if (isset($data['rule']['conditions'])) {
                    $data['conditions'] = $data['rule']['conditions'];
                }

                if (isset($data['rule']['actions'])) {
                    $data['actions'] = $data['rule']['actions'];
                }

                unset($data['rule']);

                $model->loadPost($data);

                if ($isHourDate) {
                    $model->setData('to_date', $data['to_date']);
                }

                $useAutoGeneration = (int)!empty($data['use_auto_generation']);

                $model->setUseAutoGeneration($useAutoGeneration);

                $session->setPageData($model->getData());

                $model->save();

                $session->addSuccess(Mage::helper('salesrule')->__('The rule has been saved.'));
                $session->setPageData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));

                    return;
                }

                $this->_redirect('*/*/');

                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());

                $id = (int)$this->getRequest()->getParam('rule_id');

                if (!empty($id)) {
                    $this->_redirect('*/*/edit', array('id' => $id));
                } else {
                    $this->_redirect('*/*/new');
                }

                return;
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('catalogrule')->__(
                        'An error occurred while saving the rule data. Please review the log and try again.'
                    ));
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->setPageData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('rule_id')));

                return;
            }
        }

        $this->_redirect('*/*/');
    }
}
